//
//  ViewController.swift
//  floatingButtonPanGestures
//
//  Created by Ahmad Nur Alifulloh on 05/04/22.
//

import UIKit

class ViewController: UIViewController {
    var xPosition : CGFloat!
    var yPosition : CGFloat!
    
    private let floatingButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .systemPink
        
        let image = UIImage(systemName: "plus", withConfiguration: UIImage.SymbolConfiguration(pointSize: 32, weight: .medium))
        
        button.setImage(image, for: .normal)
        button.tintColor = .white
        button.setTitleColor(.white, for: .normal)
        button.layer.shadowRadius = 10
        button.layer.shadowOpacity = 0.4
        
        //Corner Radius
        //        button.layer.masksToBounds = true
        button.layer.cornerRadius = 30
        
        return button
    }()
    
    let card : UIView = {
        let view = UIView()
        
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        xPosition = view.frame.size.width - 100
        yPosition = view.frame.size.height - 100
        
        floatingButton.frame = CGRect(x: xPosition, y: yPosition, width: 60, height: 60)
        
        view.addSubview(floatingButton)
        floatingButton.translatesAutoresizingMaskIntoConstraints = false
        floatingButton.heightAnchor.constraint(equalToConstant: 60).isActive = true
        floatingButton.widthAnchor.constraint(equalToConstant: 60).isActive = true
        
        
        floatingButton.addSubview(card)
        card.translatesAutoresizingMaskIntoConstraints = false
        card.heightAnchor.constraint(equalToConstant: 60).isActive = true
        card.widthAnchor.constraint(equalToConstant: 60).isActive = true
        
        card.isUserInteractionEnabled = false
        floatingButton.isUserInteractionEnabled = true
        floatingButton.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(self.handlePanGestures)))
        floatingButton.addTarget(self, action: #selector(didTapButton), for: .touchUpInside)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }
    
    @objc private func didTapButton(){
        let alert = UIAlertController(title: "Add Something", message: "Floating button  PanGestures", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
        present(alert, animated: true)
    }
    
    @objc func handlePanGestures(gesture: UIPanGestureRecognizer){
        let location = gesture.location(in: self.view)
        let draggedView = gesture.view
        draggedView?.center = location
        
        if gesture.state == .began{
            print("began")
            
        }
        //        else if gesture.state == .changed{
        //            print("changed")
        //            let translation = gesture.translation(in: self.view)
        //            floatingButton.transform = CGAffineTransform(translationX: translation.x, y: translation.y)
        //        }
        else if gesture.state == .ended{
            print("ended")
            if self.floatingButton.frame.midX >= self.view.layer.frame.width / 2 {
                UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
                    self.floatingButton.center.x = self.view.layer.frame.width - 40
                }, completion: nil)
            }else{
                UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
                    self.floatingButton.center.x = 40
                }, completion: nil)
            }
        }
    }
    
    
}

